# Codebase release 0.3 for ITensor

by Matthew Fishman, Steven R. White, E. Miles Stoudenmire

SciPost Phys. Codebases 4-r0.3 (2022) - published 2022-08-23

[DOI:10.21468/SciPostPhysCodeb.4-r0.3](https://doi.org/10.21468/SciPostPhysCodeb.4-r0.3)

This repository archives a fixed release as a citable refereed publication. Please refer to the [publication page](https://scipost.org/SciPostPhysCodeb.4-r0.3) to view the full bundle and get information on how to cite it.

For a link to the latest/development version, check the Resources section below.

Copyright Matthew Fishman, Steven R. White, E. Miles Stoudenmire

This README is published under the terms of the CC BY 4.0 license. For the license to the actual codebase, please refer to the license specification in the codebase folder.

## Resources:

* Codebase release version (archive) repository at [https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.4-r0.3](https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.4-r0.3)
* Live (external) repository at [https://github.com/ITensor/ITensors.jl/releases/tag/v0.3.19](https://github.com/ITensor/ITensors.jl/releases/tag/v0.3.19)
